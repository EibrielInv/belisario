# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

import bpy
import os

bl_info = {
    "name": "Belisario Panoramic",
    "author": "Oscurart, Eibriel",
    "version": (0, 1),
    "blender": (2, 81, 0),
    "location": "View3D > Sidebar > Tool",
    "description": "Add panoramic render functionality to Eevee",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Render"}


class belisarioRenderEeveePano (bpy.types.Operator):
    """Renders a panoramic image using Eevee"""
    bl_idname = "render.eeveepano"
    bl_label = "Panoramic render with Eevee"
    # bl_options = {}

    animation : bpy.props.BoolProperty(name="Animation", default=False)

    @classmethod
    def poll(cls, context):
        return True

    def create_rig(self, context):
        D = bpy.data
        C = context
        esc = C.scene
        # Create empty
        bpy.ops.object.empty_add(type='SPHERE', location=(0, 0, 0))
        empty = C.active_object
        empty.name = "beliramic_rig_empty"
        empty["belirami_object"] = True
        bpy.ops.object.constraint_add(type='COPY_TRANSFORMS')
        empty.constraints["Copy Transforms"].target = esc.objects[esc.beliramic_camera]
        # Create camera

        for camera_name in self.cameras:
            rotation = self.cameras[camera_name]
            bpy.ops.object.camera_add(enter_editmode=False, align='WORLD', location=(0, 0, 0), rotation=rotation)
            camera_obj = C.active_object
            camera_obj.name = "beliramic_camera_{}".format(camera_name)
            camera_obj["belirami_object"] = True
            camera_obj.data.angle = 1.570796  # TODO
            # Link camera to empty
            camera_obj.parent = empty

    def set_render_settings(self, context):
        D = bpy.data
        C = context
        esc = C.scene
        esc.render.engine = "BLENDER_EEVEE"
        esc.render.resolution_x = (esc.beliramic_resolution_x / 4) * esc.beliramic_quality
        esc.render.resolution_y = (esc.beliramic_resolution_y / 2) * esc.beliramic_quality
        esc.render.resolution_percentage = 100

    def render_cameras(self, context):
        D = bpy.data
        C = context
        esc = C.scene
        original_camera = esc.camera
        original_file_format = esc.render.image_settings.file_format
        original_color_mode = esc.render.image_settings.color_mode
        original_color_depth = esc.render.image_settings.color_depth
        original_exr_codec = esc.render.image_settings.exr_codec
        #
        esc.render.image_settings.file_format = 'OPEN_EXR'
        esc.render.image_settings.color_mode = 'RGBA'
        esc.render.image_settings.color_depth = '32'
        esc.render.image_settings.exr_codec = 'ZIP'

        self.images_to_delete = []
        for camera_name in self.cameras:
            cam_name = "beliramic_camera_{}".format(camera_name)
            esc.camera = D.objects[cam_name]
            if self.animation:
                filename = "{}_#####.exr".format(camera_name)
            else:
                filename = "{}.exr".format(camera_name)
            native = bpy.path.native_pathsep(esc.beliramic_outputpath)
            render_path = os.path.join(native, filename)
            bpy.context.scene.render.filepath = render_path
            self.images_to_delete.append(bpy.path.abspath(render_path))
            print("Beliramic: Rendering {}".format(camera_name))
            # must_continue = self.set_render_range(context)
            if self.should_render(context, bpy.context.scene.render.filepath):
                bpy.ops.render.render(
                    animation=self.animation,
                    write_still=True,
                    use_viewport=False
                )
            else:
                print("Skipping, file exists")
        #
        esc.camera = original_camera
        esc.render.image_settings.file_format = original_file_format
        esc.render.image_settings.color_mode = original_color_mode
        esc.render.image_settings.color_depth = original_color_depth
        esc.render.image_settings.exr_codec = original_exr_codec

    def create_equi(self, context):
        D = bpy.data
        C = context
        esc = C.scene
        equi_pos = (2.35445, 7.23125, -2.40984)
        original_camera = esc.camera
        original_word = esc.world
        original_engine = esc.render.engine
        original_film_transparent = esc.render.film_transparent
        original_file_format = esc.render.image_settings.file_format
        original_color_mode = esc.render.image_settings.color_mode
        original_color_depth = esc.render.image_settings.color_depth
        # Set to Cycles
        esc.render.engine = "CYCLES"
        esc.cycles.device = "CPU"
        esc.cycles.shading_system = True
        esc.render.image_settings.file_format = 'PNG'
        esc.render.image_settings.color_mode = 'RGB'
        esc.render.image_settings.color_depth = '16'
        #
        if "beliramic_equicamera" not in esc.objects:
            bpy.ops.object.camera_add(enter_editmode=False, align='WORLD', location=equi_pos, rotation=(0, 0, 0))
            camera_obj = C.active_object
            camera_obj.name = "beliramic_equicamera"
            camera_obj["belirami_object"] = True
        else:
            camera_obj = D.objects["beliramic_equicamera"]
        esc.camera = camera_obj
        camera_obj.data.clip_start = 0.001
        camera_obj.data.clip_end = 0.002
        #
        # addon_file = os.path.realpath(__file__)
        # addon_directory = os.path.dirname(addon_file)
        if "cubic2equirectangular.osl" not in D.texts:
            text_osl = D.texts.new("cubic2equirectangular.osl")
        else:
            text_osl = D.texts["cubic2equirectangular.osl"]
        text_osl.from_string(cubic2equirectangular_osl)
        #
        faces = ["Top", "Down", "Left", "Right", "Front", "Back"]
        if "beliramic_equicube" not in D.worlds:
            equi_world = bpy.data.worlds.new("beliramic_equicube")

            esc.world = equi_world

            equi_world.use_nodes = True
            world_nodes = equi_world.node_tree.nodes
            world_links = equi_world.node_tree.links

            # Take main nodes
            output = world_nodes['World Output']
            background = world_nodes['Background']

            # Create Color Script
            script_color = world_nodes.new('ShaderNodeScript')
            script_color.name = "script_color"
            script_color.mode = "INTERNAL"
            script_color.script = text_osl
            # script_color.filepath = "/home/gabriel/Documentos/Trabajo/Belisario/Repositorio/belisario-addons/RepositorioCodigo/belisario/cubic2equirectangular.osl"
            # bpy.ops.node.shader_script_update()
            world_links.new(script_color.outputs['Color'], background.inputs['Color'])

            # Create UV Script
            script_uv = world_nodes.new('ShaderNodeScript')
            script_uv.name = "script_uv"
            script_uv.mode = "INTERNAL"
            script_uv.script = text_osl
            # script_uv.filepath = "/home/gabriel/Documentos/Trabajo/Belisario/Repositorio/belisario-addons/RepositorioCodigo/belisario/cubic2equirectangular.osl"
            # bpy.ops.node.shader_script_update()
            # world_links.new(script_color.outputs['Color'], background.inputs['Color'])

            # Create Texture Coordinate Node
            tex_coord = world_nodes.new("ShaderNodeTexCoord")
            world_links.new(tex_coord.outputs["Window"], script_uv.inputs["Cubic"])
            world_links.new(tex_coord.outputs["Window"], script_color.inputs["Cubic"])

            # Create Image Texture Nodes
            for cube_camera in faces:
                image_node = world_nodes.new('ShaderNodeTexImage')
                image_node.name = "image_{}".format(cube_camera)

                # world_links.new(script_uv.outputs["Equirectangular"], image_node.inputs["Vector"])
                # world_links.new(image_node.outputs["Color"], script_color.inputs[cube_camera])
        else:
            esc.world = D.worlds["beliramic_equicube"]
        #
        # Link Image Texture Nodes
        world_nodes = esc.world.node_tree.nodes
        world_links = esc.world.node_tree.links
        script_uv = world_nodes["script_uv"]
        script_color = world_nodes["script_color"]
        for cube_camera in faces:
            image_node = world_nodes["image_{}".format(cube_camera)]
            world_links.new(script_uv.outputs["Equirectangular"], image_node.inputs["Vector"])
            world_links.new(image_node.outputs["Color"], script_color.inputs[cube_camera])
            native = bpy.path.native_pathsep(esc.beliramic_outputpath)
            if self.animation:
                filename = "{}_{:05d}.exr".format(cube_camera, esc.frame_start)
                img = bpy.data.images.load(os.path.join(native, filename))
                img.name = cube_camera
                image_node.image = img
                image_node.extension = "EXTEND"
                img.source = 'SEQUENCE'
                image_node.image_user.frame_duration = 10000
                image_node.image_user.frame_start = 1
                image_node.image_user.frame_offset = 0
                image_node.image_user.use_auto_refresh = True
            else:
                filename = "{}.exr".format(cube_camera)
                img = bpy.data.images.load(os.path.join(native, filename))
                img.name = cube_camera
                image_node.image = img
                image_node.extension = "EXTEND"
        #
        esc.cycles.max_bounces = 0
        esc.cycles.samples = 6
        esc.render.resolution_x = esc.beliramic_resolution_x
        esc.render.resolution_y = esc.beliramic_resolution_y
        esc.render.tile_x = 512
        esc.render.tile_y = 512
        esc.render.film_transparent = False
        if self.animation:
            filename = "equi_#####.png"
        else:
            filename = "equi.png"
        native = bpy.path.native_pathsep(esc.beliramic_outputpath)
        bpy.context.scene.render.filepath = os.path.join(native, filename)
        print("Beliramic: Rendering Equirectangular")
        if self.should_render(context, bpy.context.scene.render.filepath):
            bpy.ops.render.render(
                animation=self.animation,
                write_still=True,
                use_viewport=False
            )
        else:
            print("Skipping, file exists")
        #
        original_view_transform = esc.view_settings.view_transform
        original_exposure = bpy.context.scene.view_settings.exposure
        original_gama = bpy.context.scene.view_settings.gamma
        esc.view_settings.view_transform = 'Raw'
        bpy.context.scene.view_settings.exposure = 0
        bpy.context.scene.view_settings.gamma = 1.0
        for cube_camera in faces:
            image_node = world_nodes["image_{}".format(cube_camera)]
            # world_links.new(script_uv.outputs["Equirectangular"], image_node.inputs["Vector"])
            world_links.new(image_node.outputs["Alpha"], script_color.inputs[cube_camera])
        if self.animation:
            filename = "equi_alpha_#####.png"
        else:
            filename = "equi_alpha.png"
        native = bpy.path.native_pathsep(esc.beliramic_outputpath)
        bpy.context.scene.render.filepath = os.path.join(native, filename)
        print("Beliramic: Rendering Equirectangular Alpha")
        # must_continue = self.set_render_range(context)
        if self.should_render(context, bpy.context.scene.render.filepath):
            bpy.ops.render.render(
                animation=self.animation,
                write_still=True,
                use_viewport=False
            )
        else:
            print("Skipping, file exists")
        esc.view_settings.view_transform = original_view_transform
        bpy.context.scene.view_settings.exposure = original_exposure
        bpy.context.scene.view_settings.gamma = original_gama
        #
        esc.camera = original_camera
        esc.world = original_word
        esc.render.engine = original_engine
        esc.render.film_transparent = original_film_transparent
        esc.render.image_settings.file_format = original_file_format
        esc.render.image_settings.color_mode = original_color_mode
        esc.render.image_settings.color_depth = original_color_depth
        #
        print("Removing temporal files")
        if self.animation:
            to_delete = []
            for frame_number in range(esc.frame_start, esc.frame_end + 1):
                for camera_to_delete in self.images_to_delete:
                    camera_to_delete = camera_to_delete.replace("#####", "{:05d}")
                    camera_to_delete = camera_to_delete.format(frame_number)
                    to_delete.append(camera_to_delete)
            self.images_to_delete = to_delete

        print(self.images_to_delete)
        if not esc.beliramic_keeptemporalfiles:
            for image_to_delete in self.images_to_delete:
                os.remove(image_to_delete)

    def create_ao(self, context):
        D = bpy.data
        C = context
        esc = C.scene
        #
        original_engine = esc.render.engine
        original_film_transparent = esc.render.film_transparent
        original_file_format = esc.render.image_settings.file_format
        original_color_mode = esc.render.image_settings.color_mode
        original_color_depth = esc.render.image_settings.color_depth
        #
        esc.render.engine = "CYCLES"
        esc.cycles.device = "GPU"
        esc.cycles.shading_system = False
        esc.render.image_settings.file_format = 'PNG'
        esc.render.image_settings.color_mode = 'RGB'
        esc.render.image_settings.color_depth = '16'
        #
        if "beliramic_ao" not in D.materials:
            ao_material = bpy.data.materials.new("beliramic_ao")
            ao_material.use_nodes = True
            mat_nodes = ao_material.node_tree.nodes
            mat_links = ao_material.node_tree.links
            # a new material node tree already has a diffuse and material output node
            output = mat_nodes['Material Output']
            diffuse = mat_nodes['Principled BSDF']
            mat_nodes.remove(diffuse)
            #
            emission = mat_nodes.new('ShaderNodeEmission')
            mat_links.new(emission.outputs['Emission'], output.inputs['Surface'])
            #
            ambient_occlusion = mat_nodes.new('ShaderNodeAmbientOcclusion')
            mat_links.new(ambient_occlusion.outputs['Color'], emission.inputs['Color'])
        else:
            ao_material = bpy.data.materials["beliramic_ao"]
        #
        esc.camera.data.type = "PANO"
        esc.camera.data.cycles.panorama_type = 'EQUIRECTANGULAR'
        #
        esc.cycles.max_bounces = 0
        esc.cycles.samples = 6
        esc.render.resolution_x = esc.beliramic_resolution_x
        esc.render.resolution_y = esc.beliramic_resolution_y
        esc.render.tile_x = 512
        esc.render.tile_y = 512
        esc.view_layers[0].material_override = ao_material
        # esc.render.film_transparent = True
        if self.animation:
            filename = "ao_#####.png"
        else:
            filename = "ao.png"
        native = bpy.path.native_pathsep(esc.beliramic_outputpath)
        bpy.context.scene.render.filepath = os.path.join(native, filename)
        print("Beliramic: Rendering Ambient Occlusion")
        # must_continue = self.set_render_range(context)
        if self.should_render(context, bpy.context.scene.render.filepath):
            bpy.ops.render.render(
                animation=self.animation,
                write_still=True,
                use_viewport=False
            )
        else:
            print("Skipping, file exists")
        #
        esc.view_layers[0].material_override = None
        esc.render.engine = original_engine
        esc.render.film_transparent = original_film_transparent
        esc.render.image_settings.file_format = original_file_format
        esc.render.image_settings.color_mode = original_color_mode
        esc.render.image_settings.color_depth = original_color_depth

    def should_render(self, context, filepath):
        if self.animation:
            return True
        esc = context.scene
        print("\nskip:", esc.beliramic_skipexisting)
        print("exists:", os.path.exists(bpy.path.abspath(filepath)))
        if not esc.beliramic_skipexisting:
            return True
        if not os.path.exists(bpy.path.abspath(filepath)):
            return True
        return False

    def set_render_range(self, context):
        if not self.animation:
            return False
        wm = context.window_manager
        esc = context.scene
        start = esc.frame_start
        end = esc.frame_end
        step_size = esc.beliramic_eeveesteps
        step = wm.beliramic_currentstep
        must_continue = True

        beliramic_start = start + (step * step_size)
        beliramic_end = (start + ((step + 1) * step_size)) - 1
        if beliramic_end >= end:
            beliramic_end = end
            must_continue = False
        print("start: ", beliramic_start)
        print("end: ", beliramic_end)
        print("must_continue: ", must_continue)
        esc.frame_start = beliramic_start
        esc.frame_end = beliramic_end
        return must_continue

    def clear_objects(self, context):
        D = bpy.data
        C = context
        esc = C.scene
        obj_list = []
        for obj in C.scene.objects:
            obj_list.append(obj)
        for obj in obj_list:
            if "belirami_object" in obj:
                D.objects.remove(obj)

    def modal(self, context, event):
        # print("Modal")
        wm = context.window_manager
        if event.type in {'ESC'}:
            self.cancel(context)
            return {'CANCELLED'}

        if event.type == 'TIMER':
            self.remove_timer(context)
            D = bpy.data
            esc = context.scene
            wm.progress_update(wm.beliramic_currentstep)
            original_start = esc.frame_start
            original_end = esc.frame_end
            original_use_overwrite = esc.render.use_overwrite
            esc.render.use_overwrite = not esc.beliramic_skipexisting
            if wm.beliramic_currentstep == 0:
                self.clear_objects(context)
            #
            must_continue = self.set_render_range(context)
            self.create_rig(context)
            self.set_render_settings(context)
            self.render_cameras(context)
            self.create_equi(context)
            if esc.beliramic_aopass:
                self.create_ao(context)
            if not must_continue:
                self.clear_objects(context)
            #
            esc.frame_start = original_start
            esc.frame_end = original_end
            esc.render.use_overwrite = original_use_overwrite
            if must_continue:
                wm.beliramic_currentstep += 1
                print("Setting timer")
                self.set_timer(context)
                print("Timer set")
                return {'PASS_THROUGH'}
            else:
                return {'FINISHED'}

        # print(event.type)
        return {'PASS_THROUGH'}

    def execute(self, context):
        D = bpy.data
        C = context
        esc = C.scene
        wm = context.window_manager
        self.cameras = {
            "Front": (0., 0., 0.),
            "Back": (-3.141593, 0., 3.141593),
            "Left": (1.570796, 1.570796, 1.570796),
            "Right": (1.570796, -1.570796, -1.570796),
            "Top": (1.570796, 0., 0.),
            "Down": (1.570796, 3.141593, -3.141593)
        }
        if esc.beliramic_camera == "":
            self.report({'ERROR'}, "No camera selected")
            return {'CANCELLED'}
        camera = D.objects[esc.beliramic_camera]
        if camera.type != "CAMERA":
            self.report({'ERROR'}, "The selected camera object is not type camera")
            return {'CANCELLED'}
        wm.beliramic_currentstep = 0
        #
        self.set_timer(context)
        wm.modal_handler_add(self)
        wm.progress_begin(0, 100)
        return {'RUNNING_MODAL'}

    def set_timer(self, context):
        wm = context.window_manager
        self._timer = wm.event_timer_add(0.1, window=context.window)

    def remove_timer(self, context):
        wm = context.window_manager
        wm.event_timer_remove(self._timer)

    def cancel(self, context):
        self.clear_objects(context)
        wm = context.window_manager
        wm.event_timer_remove(self._timer)


class PROPERTIES_PT_render_BELIRAMIC_panel (bpy.types.Panel):
    """Adds a panel on Render Properties"""
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "render"
    bl_label = "Belisario Panoramic"

    def draw(self, context):
        esc = context.scene
        layout = self.layout
        layout.use_property_split = True
        row = layout.row()
        # row.label(text="Belisario Panoramic")
        props = row.operator("render.eeveepano", text="Render panoramic image")
        props.animation = False
        props = layout.operator("render.eeveepano", text="Render panoramic animation")
        props.animation = True
        layout.prop_search(esc, "beliramic_camera", esc, "objects")

        col = layout.column(align=True)
        col.prop(esc, "beliramic_resolution_x", text="Resolution X")
        col.prop(esc, "beliramic_resolution_y", text="Y")

        col = layout.column()
        col.prop(esc, "beliramic_quality", text="Render quality")
        col.prop(esc, "beliramic_skipexisting", text="Skip existing files (animation)")
        col.prop(esc, "beliramic_keeptemporalfiles", text="Keep temporal files")

        col = layout.column()
        col.prop(esc, "beliramic_outputpath", text="Output")
        col.prop(esc, "beliramic_aopass", text="AO")
        col.prop(esc, "beliramic_cryptomatte", text="Cryptomatte")

        col = layout.column()
        col.label(text="Avanzado")
        col.prop(esc, "beliramic_eeveesteps", text="Eevee steps")


classes = [
    belisarioRenderEeveePano,
    PROPERTIES_PT_render_BELIRAMIC_panel
]


def register():
    for class_ in classes:
        bpy.utils.register_class(class_)

    bpy.types.Scene.beliramic_camera = bpy.props.StringProperty(
        name="Panoramic Eevee camera"
    )
    bpy.types.Scene.beliramic_resolution_x = bpy.props.IntProperty(
        name="Panoramic Eevee resolution X",
        default=4096,
        min=4096,
        subtype="PIXEL"
    )
    bpy.types.Scene.beliramic_resolution_y = bpy.props.IntProperty(
        name="Panoramic Eevee resolution Y",
        default=2048,
        min=2048,
        subtype="PIXEL"
    )
    bpy.types.Scene.beliramic_outputpath = bpy.props.StringProperty(
        name="Panoramic Eevee output path",
        default="//",
        subtype="DIR_PATH"
    )
    bpy.types.Scene.beliramic_quality = bpy.props.IntProperty(
        name="Panoramic Eevee Render quality",
        description="Multiplicator for individual camera render resolution",
        default=2,
        min=1,
        max=4
    )
    bpy.types.Scene.beliramic_skipexisting = bpy.props.BoolProperty(
        name="Panoramic Eevee Skip existing images",
        default=False
    )
    bpy.types.Scene.beliramic_keeptemporalfiles = bpy.props.BoolProperty(
        name="Panoramic Eevee Keep temporal files",
        default=False
    )
    bpy.types.Scene.beliramic_aopass = bpy.props.BoolProperty(
        name="Panoramic Eevee Ambient Oclussion pass",
        default=True
    )
    bpy.types.Scene.beliramic_cryptomatte = bpy.props.BoolProperty(
        name="Panoramic Eevee Ambient Cryptomatte pass",
        default=True
    )
    bpy.types.Scene.beliramic_eeveesteps = bpy.props.IntProperty(
        name="Panoramic Eevee Ambient Eevee steps",
        default=10
    )
    #
    bpy.types.WindowManager.beliramic_currentstep = bpy.props.IntProperty(
        name="Panoramic Eevee current render step",
        default=0,
        options={'HIDDEN', 'SKIP_SAVE'}
    )


def unregister():
    for class_ in classes:
        bpy.utils.unregister_class(class_)


if __name__ == "__main__":
    register()


cubic2equirectangular_osl = """
#define CC_TOP       0
#define CC_LEFT      1
#define CC_FRONT     2
#define CC_RIGHT     3
#define CC_BACK      4
#define CC_DOWN      5
#define CC_FACE_NUM  6

#define S_RADIUS    1.0

vector normalize_xy(vector UV)
{
    return (UV * vector(2, 2, 2)) - vector(1, 1, 1);
}

vector spherical_coordinates(vector UV)
{
    vector spherical;
    float x = UV[0];
    float y = UV[1];

    spherical[0] = x*M_PI;         // theta - 0 to 2 pi
    spherical[1] = y*M_PI_2;   // phi -  -pi/2 (south pole) to pi/2 (north pole)
    spherical[2] = 1.0;
    return spherical;
}


vector texture_coordinates(vector spherical)
{
    vector texture_coords;
    float theta = spherical[0];
    float phi = spherical[1];

    texture_coords[0] = cos(phi) * cos(theta);
    texture_coords[1] = sin(phi);
    texture_coords[2] = cos(phi) * sin(theta);

    return texture_coords;
}


int get_face(vector spherical)
{
    float theta_norm;
    int cube_face;
    float t_theta = spherical[0];
    float t_phi = spherical[1];

    // Looking at the cube from top down
    // FRONT zone
    if (t_theta > -M_PI_4 and t_theta < M_PI_4) {
        cube_face = CC_FRONT;
        theta_norm = t_theta;
    } // LEFT zone
    else if (t_theta > -(M_PI_2 + M_PI_4) and t_theta < -M_PI_4) {
        cube_face  = CC_LEFT;
        theta_norm = t_theta + M_PI_2;
    } // RIGHT zone
    else if (t_theta > M_PI_4 and t_theta < (M_PI_2 + M_PI_4)) {
        cube_face  = CC_RIGHT;
        theta_norm = t_theta - M_PI_2;
    }
    else {
        cube_face  = CC_BACK;
        theta_norm = t_theta + ((t_theta > 0.0) ? -M_PI : M_PI);
    }

    float phi_threshold = atan2(S_RADIUS, S_RADIUS / cos(theta_norm));

    if (t_phi > phi_threshold) {
        cube_face  = CC_DOWN;
    } else if (t_phi < -phi_threshold) {
        cube_face  = CC_TOP;
    }
    return cube_face ;
}


vector cc_rot_rad(float x, float y, float rad, float temp)
{
    temp = x;
    x = x * cos(rad) - y * sin(rad);
    y = temp * sin(rad) + y * cos(rad);
    return vector(x, y, 1.0);
}


float cc_trans_dis(float axis, float dis)
{
    return axis += dis;
}


color cc_loc(float axis, float px, float py, float rad, int face)
{
    // ctx->radius = ((double)px_in_w) / 2.0;
    float radius = 1.0 / 2.0;

    float size_ratio = radius / axis;

    float cube_x = size_ratio * px;
    float cube_y = size_ratio * py;

    // rotate pi
    vector res = cc_rot_rad(cube_x, cube_y, rad, size_ratio);
    cube_x = res[0];
    cube_y = res[1];

    // translate
    cube_x = cc_trans_dis(cube_x, radius);
    cube_y = cc_trans_dis(cube_y, radius);

    return color(cube_x, cube_y, 0.0);
}


shader simple_material(
    color Top = color(1, 1, 1),
    color Down = color(1, 1, 1),
    color Left = color(1, 1, 1),
    color Right = color(1, 1, 1),
    color Front = color(1, 1, 1),
    color Back = color(1, 1, 1),
    vector Cubic = vector(1, 1, 1),
    output vector Equirectangular = vector(1, 1, 1),
    output color Color = color(1, 1, 1))
{

    vector CubicN = normalize_xy(Cubic);

    vector spherical = spherical_coordinates(CubicN);

    vector texture_coords = texture_coordinates(spherical);

    if (1 == 0) {
        Equirectangular = (texture_coords + vector(1, 1, 1)) * vector(0.5, 0.5, 0.5);
        //Equirectangular = texture_coords;
        //Equirectangular = spherical;
        //Equirectangular = CubicN;
    }

    int face = get_face(spherical);

    float t_x = texture_coords[0];
    float t_y = texture_coords[1];
    float t_z = texture_coords[2];

    if (face == CC_TOP) {
        Equirectangular = cc_loc(t_y, t_z, t_x, M_PI, face);
    } else if (face == CC_DOWN)
    {
        Equirectangular = cc_loc(t_y, t_x, t_z, -M_PI_2, face);
    } else if (face == CC_LEFT)
    {
        Equirectangular = cc_loc(t_z, t_x, t_y, M_PI, face);
    } else if (face == CC_RIGHT)
    {
        Equirectangular = cc_loc(t_z, t_y, t_x, M_PI_2, face);
    } else if (face == CC_FRONT)
    {
        Equirectangular = cc_loc(t_x, t_z, t_y, 0.0, face);
    } else if (face == CC_BACK)
    {
        Equirectangular = cc_loc(t_x, t_y, t_z, -M_PI_2, face);
    }


    // Equirectangular = vector(0, 0, 0);
    if (face == CC_TOP) {
        // Color = Top;
        Color = Down;
    }
    if (face == CC_DOWN) {
        // Color = Down;
        Color = Top;
    }
    if (face == CC_LEFT) {
        Color = Left;
    }
    if (face == CC_RIGHT) {
        Color = Right;
    }
    if (face == CC_FRONT) {
        Color = Front;
    }
    if (face == CC_BACK) {
        Color = Back;
    }


}
"""
