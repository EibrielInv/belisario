# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

import os
import re
import bpy
import sys
import json
import shutil
import pathlib
import hashlib
import datetime


script_file = os.path.realpath(__file__)
addon_path = os.path.dirname(script_file)

if addon_path not in sys.path:
    sys.path.append(addon_path)

from blender_asset_tracer.pack import Packer
# from .blender_asset_tracer.pack import Packer
# import blender_asset_tracer

bl_info = {
    "name": "Belisario Coordinacion",
    "author": "Eibriel",
    "version": (0, 1),
    "blender": (2, 81, 0),
    "location": "View3D > Sidebar > Tool",
    "description": "Add panoramic render functionality to Eevee",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "System"}


positions = [
    ('zero', 'Zero', '', 1),
    ('cursor', 'Cursor', '', 2),
]


def refreshLibrariesCallback(prop, context):
    pass


def checkNamesCallback(prop, context):
    pass


def consolidatehistory(history):
    consolidated = {}
    for version in history:
        for action in version["actions"]:
            if action["modification"] != "DELETED":
                consolidated[action["from"]] = action
            else:
                if action["from"] in consolidated:
                    del consolidated[action["from"]]
    consolidated_actions = []
    for file_ in consolidated:
        consolidated_actions.append(consolidated[file_])
    return consolidated_actions


def clearTaskChanges(context):
    wm = context.window_manager
    wm.belisario_repo_taskfiles.clear()
    wm.belisario_artist_taskfiles.clear()
    wm.belisario_receive_message = "Recibiendo cambios"
    wm.belisario_send_message = "Enviando cambios"


def reloadTask(prop, context):
    clearTaskChanges(context)


tasktypes = [
    ('BLENDER', 'Blender', '', 1),
    ('PHOTOSHOP', 'Photoshop', '', 2),
    ('DAVINCI', 'DaVinci', '', 3),
    ('OTHER', 'Other', '', 4)
]


def md5(fname):
    if not os.path.exists(fname):
        return None
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


class belisarioArtistTasks(bpy.types.PropertyGroup):
    name: bpy.props.StringProperty(
        name="Name",
        default="",
        update=checkNamesCallback
    )
    description: bpy.props.StringProperty(
        name="Description",
        default=""
    )
    type: bpy.props.EnumProperty(
        items=tasktypes,
        name="Type",
        description="Type of task",
        default="BLENDER"
    )
    filepath: bpy.props.StringProperty(
        name="Filepath",
        default="",
        subtype="FILE_PATH"
    )
    status: bpy.props.StringProperty(
        name="Status",
        default=""
    )


class belisarioArtistProperties(bpy.types.PropertyGroup):
    name: bpy.props.StringProperty(
        name="Name",
        default="",
        update=checkNamesCallback
    )
    email: bpy.props.StringProperty(
        name="Email",
        default=""
    )
    description: bpy.props.StringProperty(
        name="Description",
        default=""
    )
    sharedfolder: bpy.props.StringProperty(
        name="Shared Folder",
        default="",
        subtype="DIR_PATH"
    )
    tasks: bpy.props.CollectionProperty(
        type=belisarioArtistTasks
    )


class belisarioTaskFiles(bpy.types.PropertyGroup):
    to: bpy.props.StringProperty(
        name="To",
        default=""
    )
    from_: bpy.props.StringProperty(
        name="From",
        default=""
    )
    send: bpy.props.BoolProperty(
        name="Send",
        default=False
    )
    receive: bpy.props.BoolProperty(
        name="Receive",
        default=False
    )
    modification: bpy.props.StringProperty(
        name="Modification",
        default=""
    )


class TEXT_PT_BELIRNATION_panel (bpy.types.Panel):
    """Adds a panel on Text Editor"""
    bl_space_type = 'TEXT_EDITOR'
    bl_region_type = 'UI'
    bl_category = "Belisario"
    bl_label = "Belisario Coordination"

    def draw(self, context):
        wm = context.window_manager
        esc = context.scene
        preferences = context.preferences
        addon_prefs = preferences.addons[__name__].preferences
        layout = self.layout
        col = layout.column()
        # props = col.operator("belisario.reloadconfig", text="Reload Configuration")
        if len(esc.belisario_artists) > 0:
            col.label(text="Artists")
            row = col.row()

            row.template_list(
                "UI_UL_list",
                "ui_lib_list_prop",
                esc,
                "belisario_artists",
                esc,
                "belisario_artists_index", rows=5
            )
            col = row.column(align=True)
            col.operator("belisario.coordination_addartist", icon='ADD', text="")
            col.operator("belisario.coordination_removeartist", icon='REMOVE', text="")

            row = layout.row()
            col = row.column()
            artist = esc.belisario_artists[esc.belisario_artists_index]
            artist_name = artist.name
            artist_email = artist.email
            col.prop(artist, "name", text="Name")
            col.prop(artist, "email", text="Email")
            col.prop(artist, "description", text="Description")
            col.prop(artist, "sharedfolder", text="Shared Folder")
            #
            # layout.separator_spacer()
            #
            col = layout.column()
            if len(artist.tasks) > 0:
                col.label(text="{}'s Tasks".format(artist_name))
                row = col.row()

                row.template_list(
                    "UI_UL_list",
                    "ui_lib_list_prop_",
                    artist,
                    "tasks",
                    esc,
                    "belisario_tasks_index", rows=5
                )
                col = row.column(align=True)
                col.operator("belisario.coordination_addtask", icon='ADD', text="")
                col.operator("belisario.coordination_removetask", icon='REMOVE', text="")

                col = layout.column()
                task = artist.tasks[esc.belisario_tasks_index]
                col.prop(task, "name", text="Name")
                col.prop(task, "description", text="Description")
                col.prop(task, "type", text="Type")
                col.prop(task, "filepath", text="Filepath")
                col.prop(task, "status", text="Status")
                #
                # layout.separator_spacer()
                #
                col = layout.box()
                file_path = get_file_path(task.filepath)
                project_path = get_project_path()
                target_folder = get_target_folder(artist, task.name)
                col.label(text="File to pack:")
                col.label(text=str(file_path))
                if not os.path.exists(file_path):
                    col.label(text="File to pack don't exists", icon="ERROR")
                col.label(text="Project root:")
                col.label(text=str(project_path))
                if not os.path.exists(project_path):
                    col.label(text="Project root don't exists", icon="ERROR")
                col.label(text="Destination:")
                col.label(text=target_folder)
                task_sent = False
                if os.path.exists(target_folder):
                    task_sent = True
                    col.label(text="Task already sent", icon="ERROR")
                #
                col = layout.column()
                if not task_sent:
                    col.prop(wm, "belisario_send_message")
                    # row = col.row()
                    props = col.operator("file.packblendfile", text="Pack Task")
                else:
                    props = col.operator("belisario.taskdiff", text="Check changes on task")
                    col.label(text="Files modified on repository")
                    for taskfile_item in wm.belisario_repo_taskfiles:
                        row_ = col.row()
                        row_.label(text="{} ({})".format(taskfile_item.from_, taskfile_item.modification))
                        row_.prop(taskfile_item, "send", text="")
                    # row = col.row()
                    col.prop(wm, "belisario_send_message")
                    props = col.operator("file.updateblendfile", text="Send changes to artist")
                    col.label(text="Files modified by artist")
                    for taskfile_item in wm.belisario_artist_taskfiles:
                        row_ = col.row()
                        row_.label(text="{} ({})".format(taskfile_item.to, taskfile_item.modification))
                        row_.prop(taskfile_item, "receive", text="")
                    # row = col.row()
                    col.prop(wm, "belisario_receive_message")
                    props = col.operator("file.receivefiles", text="Receive changes from artist")
                col = layout.column()
                col.label(text="History:")
                target_folder = get_target_folder(artist, task.name)
                history = load_history(target_folder)
                history.reverse();
                for commit in history:
                    box = layout.box()
                    box.label(text=commit["message"])
                    box.label(text=commit["timedate"])
                    for action in commit["actions"]:
                        if commit["actions"] == "COORD_TO_ARTIST":
                            pathtoshow = action["to"]
                            if action["modification"] == "DELETED":
                                pathtoshow = action["from"]
                            box.label(text="{} ({})".format(pathtoshow, action["modification"]))
                        else:
                            pathtoshow = action["from"]
                            if action["modification"] == "DELETED":
                                pathtoshow = action["to"]
                            box.label(text="{} ({})".format(pathtoshow, action["modification"]))
            else:
                col.operator("belisario.coordination_addtask", icon='ADD', text="Add Task")
        else:
            col.operator("belisario.coordination_addartist", icon='ADD', text="Add Artist")


def get_target_folder(artist, task_name):
    artists_folder = bpy.path.abspath(artist.sharedfolder)
    artists_folder = os.path.abspath(artists_folder)
    target_folder = os.path.join(artists_folder, task_name)
    return target_folder


def get_project_path():
    project_path = pathlib.Path(os.path.dirname(bpy.data.filepath))
    return project_path


def get_file_path(task_filepath):
    file_path = pathlib.Path(os.path.abspath(bpy.path.abspath(task_filepath)))
    return file_path


def get_config_path():
    configfile = os.path.join(os.path.dirname(bpy.data.filepath), "belisario-coordinacion-animacion.json")
    return configfile


def load_config():
    data = {"artists": []}
    configfile = get_config_path()
    # print("\nconfigfile", configfile)
    if os.path.exists(configfile):
        # print("Exists")
        with open(configfile) as data_file:
            try:
                data = json.load(data_file)
                # print(data)
            except:
                print("Error reading ", configfile)
    else:
        print("Not exists")
    return data


def save_config(data):
    configfile = get_config_path()
    with open(configfile, 'w') as data_file:
        json.dump(
            data,
            data_file,
            sort_keys=True,
            indent=4,
            separators=(',', ': ')
        )


def load_history(task_path):
    historyfile = os.path.join(task_path, "history.json")
    if os.path.exists(historyfile):
        # print("Exists")
        with open(historyfile) as data_file:
            try:
                data = json.load(data_file)
                # print(data)
            except:
                print("Error reading ", configfile)
    else:
        print("Not exists")
        return []
    return data


def save_history(history, task_path):
    historyfile = os.path.join(task_path, "history.json")
    with open(historyfile, 'w') as data_file:
        json.dump(
            history,
            data_file,
            sort_keys=True,
            indent=4,
            separators=(',', ': ')
        )


def update_history(new_version, task_path):
    history = load_history(task_path)
    history.append(new_version)
    save_history(history, task_path)


class belisarioAddArtist (bpy.types.Operator):
    """Add Artist"""
    bl_idname = "belisario.coordination_addartist"
    bl_label = "Add Artist"
    bl_options = {"REGISTER", "UNDO"}

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        esc = context.scene
        artists_set = esc.belisario_artists
        artist_item = artists_set.add()
        artist_item.name = "New Artist"
        artist_item.email = "new@artist.com"
        return {'FINISHED'}


class belisarioRemoveArtist (bpy.types.Operator):
    """Remove Artist"""
    bl_idname = "belisario.coordination_removeartist"
    bl_label = "Remove Artist"
    bl_options = {"REGISTER", "UNDO"}

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        esc = context.scene
        esc.belisario_artists.remove(esc.belisario_artists_index)
        if esc.belisario_artists_index >= len(esc.belisario_artists):
            esc.belisario_artists_index = len(esc.belisario_artists) - 1
        return {'FINISHED'}


class belisarioAddTask (bpy.types.Operator):
    """Add Artist"""
    bl_idname = "belisario.coordination_addtask"
    bl_label = "Add Task"
    bl_options = {"REGISTER", "UNDO"}

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        esc = context.scene
        artist = esc.belisario_artists[esc.belisario_artists_index]
        task_item = artist.tasks.add()
        task_item.name = "New Task"
        task_item.type = "BLENDER"
        task_item.filepath = ""
        task_item.status = ""
        return {'FINISHED'}


class belisarioRemoveTask (bpy.types.Operator):
    """Remove Artist"""
    bl_idname = "belisario.coordination_removetask"
    bl_label = "Remove Task"
    bl_options = {"REGISTER", "UNDO"}

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        esc = context.scene
        artist = esc.belisario_artists[esc.belisario_artists_index]
        artist.tasks.remove(esc.belisario_tasks_index)
        if esc.belisario_tasks_index >= len(artist.tasks):
            esc.belisario_tasks_index = len(artist.tasks) - 1
        return {'FINISHED'}


def packblend(context, update=False):
    wm = context.window_manager
    esc = context.scene
    preferences = context.preferences
    addon_prefs = preferences.addons[__name__].preferences
    artist = esc.belisario_artists[esc.belisario_artists_index]
    task = artist.tasks[esc.belisario_tasks_index]
    file_path = get_file_path(task.filepath)
    project_path = get_project_path()
    target_folder = get_target_folder(artist, task.name)
    version_history = {
        "actions": [],
        "message": "",
        "user": "",
        "direction": "COORD_TO_ARTIST",
        "timedate": 0
    }
    send_to_folder = target_folder
    if update:
        send_to_folder = send_to_folder + "_temp"
    if task.type == "BLENDER":
        bat = Packer(file_path, project_path, target=send_to_folder)
        bat.strategise()
        bat.execute()
        # Si es una actualización
        if update:
            actions = []
            for taskfile_item in wm.belisario_repo_taskfiles:
                if taskfile_item.send:
                    if taskfile_item.modification == "DELETED":  # Si se borra un archivo
                        versions_action = {
                            "from": str(taskfile_item.from_),
                            "from_md5": "",
                            "to": str(taskfile_item.to),
                            "to_md5": md5(taskfile_item.to),
                            "modification": taskfile_item.modification
                        }
                        os.remove(taskfile_item.to)
                        actions.append(versions_action)
                    else:  # Si se agrega o modifica un archivo
                        temp_path = taskfile_item.to
                        temp_path = temp_path.replace(target_folder, send_to_folder)
                        if os.path.exists(temp_path):
                            os.rename(temp_path, taskfile_item.to)
                            versions_action = {
                                "from": str(taskfile_item.from_),
                                "from_md5": md5(taskfile_item.from_),
                                "to": str(taskfile_item.to),
                                "to_md5": md5(taskfile_item.to),
                                "modification": taskfile_item.modification
                            }
                            actions.append(versions_action)
                        else:
                            print("Update error: {} does not exists".format(temp_path))
            if os.path.exists(send_to_folder) and send_to_folder != "" and send_to_folder != "/":
                shutil.rmtree(send_to_folder)
        # Si NO es una actualización
        else:
            actions = []
            for action_path in bat._actions:
                action = bat._actions[action_path]
                versions_action = {
                    "from": str(action_path),
                    "from_md5": md5(action_path),
                    "to": str(action.new_path),
                    "to_md5": md5(action.new_path),
                    "modification": "ADDED"
                }
                actions.append(versions_action)
    else:  # Si el tipo de tarea NO es Blender
        if not os.path.exists(send_to_folder):
            os.makedirs(send_to_folder)
        if os.path.isdir(file_path):
            if os.path.isdir(file_path):
                send_to_folder = os.path.join(send_to_folder, os.path.basename(file_path))
            shutil.copytree(file_path, send_to_folder)
        else:
            shutil.copy(file_path, send_to_folder)
            versions_action = {
                "from": str(file_path),
                "from_md5": md5(file_path),
                "to": str(send_to_folder),
                "to_md5": md5(send_to_folder),
                "modification": "ADDED"
            }
            actions.append(versions_action)
    version_history["actions"] = actions
    version_history["message"] = wm.belisario_send_message
    version_history["user"] = "Coordinator"
    version_history["direction"] = "COORD_TO_ARTIST"
    version_history["timedate"] = datetime.datetime.now().isoformat()
    update_history(version_history, target_folder)


class belisarioUpdateFile (bpy.types.Operator):
    """Updates a task"""
    bl_idname = "file.updateblendfile"
    bl_label = "Update blendfile"

    @classmethod
    def poll(cls, context):
        wm = context.window_manager
        for taskfile_item in wm.belisario_repo_taskfiles:
            if taskfile_item.send:
                return True
        return False

    def execute(self, context):
        packblend(context, update=True)
        clearTaskChanges(context)
        return {'FINISHED'}


class belisarioReceiveFiles (bpy.types.Operator):
    """Receive files from artist"""
    bl_idname = "file.receivefiles"
    bl_label = "Receive blendfile"

    @classmethod
    def poll(cls, context):
        wm = context.window_manager
        for taskfile_item in wm.belisario_artist_taskfiles:
            if taskfile_item.receive:
                return True
        return False

    def execute(self, context):
        version_history = {
            "actions": [],
            "message": "",
            "user": "",
            "direction": "COORD_TO_ARTIST",
            "timedate": 0
        }
        wm = context.window_manager
        esc = context.scene
        artist = esc.belisario_artists[esc.belisario_artists_index]
        task = artist.tasks[esc.belisario_tasks_index]
        target_folder = get_target_folder(artist, task.name)
        actions = []
        for taskfile_item in wm.belisario_artist_taskfiles:
            if taskfile_item.receive:
                if taskfile_item.modification == "DELETED":
                    versions_action = {
                        "from": str(taskfile_item.from_),
                        "from_md5": md5(taskfile_item.from_),
                        "to": str(taskfile_item.to),
                        "to_md5": "",
                        "modification": taskfile_item.modification
                    }
                    os.remove(taskfile_item.from_)
                    actions.append(versions_action)
                else:
                    if taskfile_item.modification == "ADDED" and os.path.exists(taskfile_item.from_):
                        self.report({'WARNING'}, "File {} already exists in repository, can't be overwrited by {}".format(pathend, taskfile_item.to))
                        continue
                    shutil.copy(taskfile_item.to, taskfile_item.from_)
                    versions_action = {
                        "from": str(taskfile_item.from_),
                        "from_md5": md5(taskfile_item.from_),
                        "to": str(taskfile_item.to),
                        "to_md5": md5(taskfile_item.to),
                        "modification": taskfile_item.modification
                    }
                    actions.append(versions_action)
        if len(actions) > 0:
            version_history["actions"] = actions
            version_history["message"] = wm.belisario_receive_message
            version_history["user"] = "Coordinator"
            version_history["direction"] = "ARTIST_TO_COORD"
            version_history["timedate"] = datetime.datetime.now().isoformat()
            update_history(version_history, target_folder)
            clearTaskChanges(context)
        return {'FINISHED'}


class belisarioPackFile (bpy.types.Operator):
    """Packs a blendfile"""
    bl_idname = "file.packblendfile"
    bl_label = "Pack blendfile"

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        packblend(context)
        return {'FINISHED'}


class belisarioDiff (bpy.types.Operator):
    """Check changes on tasks"""
    bl_idname = "belisario.taskdiff"
    bl_label = "Check changes on tasks"

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        esc = context.scene
        wm = context.window_manager
        artist = esc.belisario_artists[esc.belisario_artists_index]
        task = artist.tasks[esc.belisario_tasks_index]
        target_folder = get_target_folder(artist, task.name)
        history = load_history(target_folder)
        wm.belisario_repo_taskfiles.clear()
        wm.belisario_artist_taskfiles.clear()
        allhistory = consolidatehistory(history)
        deleted_on_artist = []
        for action in allhistory:
            from_new_md5 = md5(action["from"])
            to_new_md5 = md5(action["to"])
            if to_new_md5 is None:
                deleted_on_artist.append(action)
                taskfile_item = wm.belisario_artist_taskfiles.add()
                taskfile_item.from_ = action["from"]
                taskfile_item.to = action["to"]
                taskfile_item.modification = "DELETED"
            if from_new_md5 is None or to_new_md5 is None:
                continue
            #
            if from_new_md5 != action["from_md5"]:
                taskfile_item = wm.belisario_repo_taskfiles.add()
                taskfile_item.from_ = action["from"]
                taskfile_item.to = action["to"]
                taskfile_item.modification = "MODIFIED"
            if to_new_md5 != action["to_md5"]:
                taskfile_item = wm.belisario_artist_taskfiles.add()
                taskfile_item.from_ = action["from"]
                taskfile_item.to = action["to"]
                taskfile_item.modification = "MODIFIED"
        # Buscar archivos eliminados o agregados
        file_path = get_file_path(task.filepath)
        project_path = get_project_path()
        target_folder = get_target_folder(artist, task.name)
        bat = Packer(file_path, project_path, target=target_folder, noop=True)
        bat.strategise()
        bat.execute()
        actions = []
        for action_path in bat._actions:
            action = bat._actions[action_path]
            versions_action = {
                "from": str(action_path),
                "from_md5": md5(action_path),
                "to": str(action.new_path),
                "to_md5": md5(action.new_path)
            }
            actions.append(versions_action)
        # Deleted on repo
        deleted_on_repo = []
        for action_history in allhistory:  # Revisa estado almacenado
            exists_in_new = False
            for action_new in actions:  # Revisa estado real
                if action_history["from"] == action_new["from"]:  # Si el archivo almacenado está en el estado real
                    exists_in_new = True
                    break
            # Si el archivo almacenado no está en el estado real
            if not exists_in_new and not os.path.exists(action_history["from"]):  # TODO si no existe en el pack, y si fue eliminado por el artista
                deleted_on_repo.append(action_history)
                taskfile_item = wm.belisario_repo_taskfiles.add()
                taskfile_item.from_ = action_history["from"]
                taskfile_item.to = action_history["to"]
                taskfile_item.modification = "DELETED"
        # Added on repo
        added_on_repo = []
        for action_new in actions:  # Revisa estado real
            exists_in_history = False
            for action_history in allhistory:  # Revisa estado almacenado
                if action_history["from"] == action_new["from"]:
                    exists_in_history = True
                    break
            # Si el archivo real no está en el estado almacenado
            if not exists_in_history:
                # Create .to filepath
                project_path = get_project_path()
                pathend = os.path.relpath(action_new["from"], start=project_path)
                topath = os.path.join(target_folder, pathend)
                #
                added_on_repo.append(action_new)
                taskfile_item = wm.belisario_repo_taskfiles.add()
                taskfile_item.from_ = action_new["from"]
                taskfile_item.to = topath
                taskfile_item.modification = "ADDED"
        # Added on artist
        artist_filelist = []
        for root, dirnames, filenames in os.walk(target_folder):
            for filename in filenames:
                artist_filelist.append(os.path.join(root, filename))
        added_on_artist = []
        for artist_file in artist_filelist:
            exists_in_artist = False
            for action_history in allhistory:
                if action_history["to"] == artist_file:
                    exists_in_artist = True  # TODO: Debería ir exists_in_history
            if not exists_in_artist and not should_exclude(artist_file):
                # Create .from_ filepath
                project_path = get_project_path()
                pathend = os.path.relpath(artist_file, start=target_folder)
                frompath = os.path.join(project_path, pathend)
                #
                added_on_artist.append(artist_file)
                taskfile_item = wm.belisario_artist_taskfiles.add()
                taskfile_item.from_ = frompath
                taskfile_item.to = artist_file
                taskfile_item.modification = "ADDED"
        return {'FINISHED'}


def should_exclude(filepath):
    exclude = re.findall(r'(\.blend\d+|history\.json|pack-info\.txt)$', filepath)
    # print("exclude", exclude)
    return len(exclude) > 0


class belisarioReloadConfig (bpy.types.Operator):
    """Packs a blendfile"""
    bl_idname = "belisario.reloadconfig"
    bl_label = "Reload configuration file"

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        esc = context.scene
        artists_set = esc.belisario_artists
        artists_set.clear()
        data = load_config()
        for artist in data["artists"]:
            # print(artist["name"])
            artist_item = artists_set.add()
            artist_item.name = artist["name"]
            artist_item.email = artist["email"]
            #
            tasks_set = artist_item.tasks
            tasks_set.clear()
            for task in artist["tasks"]:
                task_item = tasks_set.add()
                task_item.name = task["name"]
                task_item.type = task["type"]
                task_item.filepath = task["filepath"]
                task_item.status = task["status"]
        return {'FINISHED'}


class belisarioCoordinationPreferences(bpy.types.AddonPreferences):
    bl_idname = __name__

    belisario_artistpath: bpy.props.StringProperty(
        name="Artists shared folders",
        default="//",
        subtype="DIR_PATH"
    )

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        col = row.column()
        layout.prop(self, "belisario_artistpath")


classes = [
    belisarioArtistTasks,
    belisarioArtistProperties,
    belisarioTaskFiles,
    belisarioRemoveTask,
    belisarioAddTask,
    belisarioRemoveArtist,
    belisarioAddArtist,
    belisarioUpdateFile,
    belisarioReceiveFiles,
    belisarioPackFile,
    belisarioDiff,
    belisarioReloadConfig,
    # belisarioCoordinationPreferences,
    TEXT_PT_BELIRNATION_panel
]


def register():
    for class_ in classes:
        bpy.utils.register_class(class_)

    bpy.types.Scene.belisario_artists_index = bpy.props.IntProperty(
        update=reloadTask
    )
    bpy.types.Scene.belisario_artists = bpy.props.CollectionProperty(
        type=belisarioArtistProperties
    )
    bpy.types.Scene.belisario_tasks_index = bpy.props.IntProperty(
        update=reloadTask
    )
    bpy.types.WindowManager.belisario_repo_taskfiles = bpy.props.CollectionProperty(
        type=belisarioTaskFiles
    )
    bpy.types.WindowManager.belisario_artist_taskfiles = bpy.props.CollectionProperty(
        type=belisarioTaskFiles
    )
    bpy.types.WindowManager.belisario_send_message = bpy.props.StringProperty(
        name="Send Message",
        default="Enviando cambios"
    )
    bpy.types.WindowManager.belisario_receive_message = bpy.props.StringProperty(
        name="Send Message",
        default="Recibiendo cambios"
    )


def unregister():
    for class_ in classes:
        bpy.utils.unregister_class(class_)


if __name__ == "__main__":
    register()
